$(document).ready(function () {

  $('.e-doctors__cards').slick({
    arrows: true,
    dots: false,
    autoplay: true,
    speed: 300
  });

  $('.e-feedback__cards').slick({
    arrows: true,
    dots: false,
    autoplay: true,
    speed: 300
  });

  $('.e-services__card-right').slick({
    arrows: true,
    dots: false,
    autoplay: true,
    speed: 300
  });

  $('.e-slider').slick({
    arrows: true,
    dots: false,
    autoplay: false,
    speed: 300
  });

  $('a[href^="#"]').on('click', function (e) {
    e.preventDefault();

    var target = this.hash,
      $target = $(target);

    $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  });

  $(window).scroll(function () {
    if ($(window).width() > 1200) {

      if ($(this).scrollTop() > 50) {
        $('.e-header').css({ 'padding': '5px 0' });
        $('.e-header__logo').css({ 'opacity': '0.9', 'margin-left': '-80px' });
        $('.e-header__logo img').css({ 'max-width': '160px' });
      } else {
        $('.e-header').css({ 'padding': '20px 0' });
        $('.e-header__logo').css({ 'opacity': '1', 'margin-left': '-139px' });
        $('.e-header__logo img').css({ 'max-width': '100%' });
      }

    }
  });

  $('form').submit(function (e) {
    e.preventDefault();
    var f = $(this);
    $('.ierror', f).removeClass('ierror');
    var person = $('input[name="person"]', f).val();
    var phone = $('input[name="phone"]', f).val();
    var error = false;
    if (person == '') {
      $('input[name="person"]', f).addClass('ierror');
      error = true;
    }
    if (phone == '') {
      $('input[name="phone"]', f).addClass('ierror');
      error = true;
    }
    if (error) {
      return false;
    }
    $.ajax({
      type: 'POST',
      url: 'mail.php',
      data: $(this).serialize(),
    }).done(function (data) {
      if (data.response == 'ok') {
        alert('Спасибо! Ваша заявка отправлена');
      }
      else {
        alert('Ошибка! Заявка не отправлена, повторите запрос позже');
      }
    });
    return false;
  });

});
